package com.phpexpertstask.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "UserData")
data class UserData(
    @ColumnInfo(name = "firstName")
    var firstName : String = "",
    @ColumnInfo(name = "lastName")
    var lastName : String = "",
    @ColumnInfo(name = "mobileNo")
    var mobileNo : String = "",

    @PrimaryKey var email : String = "",

    @ColumnInfo(name = "pin")
    var pin : Int = 0
)
