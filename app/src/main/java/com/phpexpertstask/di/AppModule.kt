package com.phpexpertstask.di

import android.app.Application
import androidx.room.Room
import com.phpexpertstask.R
import com.phpexpertstask.db.MyDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun provideDataBase(application: Application): MyDatabase =
        Room.databaseBuilder(
            application,
            MyDatabase::class.java,
            application.applicationContext.getString(R.string.app_name)
        ).fallbackToDestructiveMigration().build()
}