package com.phpexpertstask.util

import java.util.regex.Pattern


    /**
     * If String is a email
     * */
    fun String.isValidEmail() : Boolean = !Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z]{2,5}" +
                ")+"
    ).matcher(this.trim())
        .matches()

