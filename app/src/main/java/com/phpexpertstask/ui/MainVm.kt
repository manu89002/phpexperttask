package com.phpexpertstask.ui

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.phpexpertstask.R
import com.phpexpertstask.db.MyDatabase
import com.phpexpertstask.model.UserData
import com.phpexpertstask.util.isValidEmail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainVm @Inject constructor(
    var myDatabase: MyDatabase
) :ViewModel(){

    var firstName = ObservableField("")
    var lastName = ObservableField("")
    var email = ObservableField("")
    var mobileNumber = ObservableField("")
    var pin = ObservableField("")
    var seeData = ObservableBoolean(false)
    var userDataList = MutableLiveData<MutableList<UserData>>()

    init {
        getUserData()
    }
    fun onClick(v: View){

        if(v.id == R.id.btnSave){

            when {
                firstName.get().toString().isEmpty() -> {
                    Toast.makeText(v.context, " Please enter first name ",Toast.LENGTH_LONG).show()
                }
                lastName.get().toString().isEmpty() -> {
                    Toast.makeText(v.context, "Please enter last name ",Toast.LENGTH_LONG).show()
                }
                mobileNumber.get().toString().isEmpty() -> {
                    Toast.makeText(v.context, "Please enter mobile number ",Toast.LENGTH_LONG).show()
                }
                mobileNumber.get().toString().length < 6 -> {
                    Toast.makeText(v.context, "Mobile number should be greater than 5 digits ",Toast.LENGTH_LONG).show()
                }
                email.get().toString().isEmpty() -> {
                    Toast.makeText(v.context, "Please enter email ",Toast.LENGTH_LONG).show()
                }
                email.get().toString().isValidEmail()->{
                    Toast.makeText(v.context, "Please enter valid email address",Toast.LENGTH_LONG).show()
                }
                pin.get().toString().isEmpty() -> {
                    Toast.makeText(v.context, "Please enter pin  ",Toast.LENGTH_LONG).show()
                }
                pin.get().toString().length != 6 -> {
                    Toast.makeText(v.context, "Pin should be 6 digits ",Toast.LENGTH_LONG).show()
                }
                else->{
                    userDataList.value?.forEach {
                        if(email.get().toString() == it.email){
                            Toast.makeText(v.context, "User Already exists ",Toast.LENGTH_LONG).show()
                            return
                        }
                    }
                    saveUserData(v)
                }
            }
        }else{
            seeData.set(!seeData.get())
            if(seeData.get()!!){

                getUserData()
            }
        }
    }

    fun getUserData() = viewModelScope.launch(Dispatchers.IO){
        userDataList.postValue(myDatabase.userData().getUser())
    }
    fun saveUserData(v: View){
        val userData = UserData().also {
            it.firstName = firstName.get()?:""
            it.lastName = lastName.get()?:""
            it.email = email.get()?:""
            it.mobileNo = mobileNumber.get()?:""
            it.pin = pin.get()?.toInt()?:0
        }
        CoroutineScope(Dispatchers.IO).launch {
            myDatabase.userData().addUser(userData)
            CoroutineScope(Dispatchers.Main).launch {
                Toast.makeText(v.context, " User Added ",Toast.LENGTH_LONG).show()
                firstName.set("")
                lastName.set("")
                mobileNumber.set("")
                email.set("")
                pin.set("")
            }
        }
    }

}