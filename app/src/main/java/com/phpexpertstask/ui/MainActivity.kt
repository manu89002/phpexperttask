package com.phpexpertstask.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import com.phpexpertstask.R
import com.phpexpertstask.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    var binding : ActivityMainBinding?=null
    val viewModel by viewModels<MainVm>()
    var adapter : SeeAllUsersAdapter? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        binding?.vm = viewModel
        adapter = SeeAllUsersAdapter()
        binding?.rvUserData?.adapter = adapter
        observeData()
    }

    private fun observeData() {
        viewModel.userDataList.observe(this){
            if(it != null){
                    Log.e("Data", " bbbbb ${it.toString()}")
                adapter?.additems(it?:ArrayList())
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}