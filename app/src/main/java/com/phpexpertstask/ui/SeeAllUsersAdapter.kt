package com.phpexpertstask.ui


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.phpexpertstask.BR
import com.phpexpertstask.R
import com.phpexpertstask.databinding.RvUserDataBinding
import com.phpexpertstask.model.UserData

class SeeAllUsersAdapter : RecyclerView.Adapter<SeeAllUsersAdapter.viewHolder>() {

    var userList : MutableList<UserData>? =null
    init {
        userList =  ArrayList()
    }

    class viewHolder(var binding:RvUserDataBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(model: UserData) {
            binding.setVariable(BR.model, model)
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val binding: RvUserDataBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.rv_user_data,
            parent,
            false
        )

        return viewHolder(binding)
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        holder.bind(userList?.get(position)!!)
    }

    override fun getItemCount(): Int {
        return userList?.size?:0
    }

    fun additems(userList : MutableList<UserData>){
        this.userList?.clear()
        this.userList?.addAll(userList)
        notifyDataSetChanged()
    }

}