package com.phpexpertstask.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.phpexpertstask.model.UserData

@Dao
interface UserDataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUser(userData : UserData)

    @Query("SELECT * FROM UserData")
    fun getUser(): MutableList<UserData>

}