package com.phpexpertstask.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.phpexpertstask.dao.UserDataDao
import com.phpexpertstask.model.UserData

@Database(
    entities = [UserData::class],
    version = 1,
    exportSchema = true
)
abstract class MyDatabase  : RoomDatabase(){
    /**
     * User Data Dao
     * */
    abstract fun userData(): UserDataDao

}